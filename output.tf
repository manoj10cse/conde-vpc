# registering the output. So that other repos can use them when creating resoruces.
output "VPC_ID" {
  value = aws_vpc.main.id
}

output "VPC_CIDR" {
  value = var.VPC_CIDR
}

output "PUBLIC_SUBNETS" {
  value = aws_subnet.public.*.id
}

