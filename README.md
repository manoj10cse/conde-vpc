#How this works ?
The whole assigment intent is to onboard a HELLO WORLD APP.

# What all I've used ?
--> Terraform-vpc //To create the network 
--> Terraform-EC2. // To create an instance which DOcker Engine to run the customised onboarding app.
Here I considered NGINX to expose the APP.

Services & Tools used:
```vpc ec2 DOCKER```

```

1) Clone the conde-vpc repo and this will setup the required network.
```
  terraform init -backend-config=config/dev-backend.tfvars
  terraform apply -auto-approve -var-file=config/dev.tfvars

```

2) Clone the conde-ec2 repo and this will setup the EC and will create a Docker Image and will run the Hello world APP.
```
  terraform init -backend-config=config/dev-backend.tfvars
  terraform apply -auto-approve -var-file=config/dev.tfvars

```

3) Use the displayed public Ip to connect to the docker app.
