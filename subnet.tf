resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.main.id
  count                   = length(var.PUB_SUBNET_CIDR)
  cidr_block              = element(var.PUB_SUBNET_CIDR , count.index)
  #availability_zone       = element(data.aws_availability_zones.available.*.names, count.index)
  availability_zone        = element(data.aws_availability_zones.zones.names, count.index)
  tags = {
    Name                  = "${var.PROJECT_NAME}-PubSubnet-${count.index+1}"
  }
}
